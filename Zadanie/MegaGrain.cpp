#include <MegaGrain.h>

std::string MegaGrain::itos(int i)
{
	std::stringstream ss;
	ss << i;
	return ss.str();
}

std::string MegaGrain::compute(std::vector<std::vector<double> > table, int timewindow)
{
	std::string text;
	
	time_window = timewindow;
	auto windows = convertToWindows(table);
	for(auto grain : windows)
	{
		Grain g;
		text += g.getText(grain) + "\n";
	}
	return text;
}

std::vector<std::vector<std::vector<double> > > MegaGrain::convertToWindows(std::vector<std::vector<double> > table)
{
	std::vector<std::vector<std::vector<double> > > vector;
	
	for(unsigned int i=0; i <= table.size() - time_window;i++)
	{
		std::vector<std::vector<double> > grain;
		for(unsigned int j=0; j < table[i].size(); j++)
		{
			std::vector<double> dimension;
			for(int k=0; k < time_window;k++)
			{
				dimension.push_back(table[i+k][j]);
			}
			
			grain.push_back(dimension);
		}
		vector.push_back(grain);
	}
	return vector;
}