#pragma once

#include <string>
#include <sstream>

class Data
{
private:
	std::string DblToStr(double d)
	{
		std::stringstream ss;
		ss.precision(10);
		ss << d;
		std::string s;
		ss >> s;
		return s;
	}
	
	double cut_last(double d)
	{
		std::stringstream ss;
		ss.precision(10);
		ss << d;
		ss >> d;
		return d;
	}
	
public:
	std::string toString()
	{
		std::string str;
		str += "\nAverage: " + DblToStr(average) + "\nQuartille1: " + DblToStr(quartille1)
		+ "\nMedian: " + DblToStr(median) + "\nQuartille3: " + DblToStr(quartille3) 
		+ "\nDeviation: " + DblToStr(deviation) + "\nKurtosis: " + DblToStr(kurtosis) + "\n";
		return str;
	}
	
	bool operator!=(const Data & d)
	{
		if(cut_last(this->average) != cut_last(d.average) || cut_last(this->quartille1) != cut_last(d.quartille1) || cut_last(this->median) != cut_last(d.median) || cut_last(this->quartille3) != cut_last(d.quartille3) || cut_last(this->deviation) != cut_last(d.deviation) || cut_last(this->kurtosis) != cut_last(d.kurtosis))
		{
			return true;
		}
		return false;
	}
	
	double average, quartille1, median, quartille3, deviation, kurtosis;
};
