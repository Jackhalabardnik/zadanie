#include <FileInput.h>

void FileInput::read(std::string s)
{
	std::fstream file;
	file.open(s);
	
	is_file_empty = !(file.good());
	std::string line;
	int sample_counter = 0;
	is_data_good = true;

	error_line.erase(error_line.begin(), error_line.end());
	sample.erase(sample.begin(),sample.end());
	whole_text.erase(whole_text.begin(),whole_text.end());
	vector.erase(vector.begin(), vector.end());

	
	while(std::getline(file,line))
	{
		if(line.size() > 0)
		{
			if(sample_counter < 20)
			{
				sample += line + "\n";
				sample_counter++;
			}
			
			whole_text += line + "\n";
			
			auto vector_of_strings = breakLine(line);
			
			for(unsigned int i = 0; i< vector.size(); i++)
			{
				if(vector[i].size() != vector_of_strings.size())
				{
					is_non_equal_number_of_dimensions = true;
					error_line = line;
					return;
				}
			}
			
			if(validateStringVector(vector_of_strings) == false)
			{
				is_data_good = false;
				error_line = line;
				return;
			}
			
			vector.push_back(convertToDouble(vector_of_strings));
		}
	}
	
	if(vector.size() == 0)
	{
		is_file_empty = true;
	}
	
	if(whole_text.size() > 1)
	{
		whole_text.erase(whole_text.end() - 1,whole_text.end());
		sample.erase(sample.end() - 1, sample.end());
	}
}

std::string FileInput::getText()
{
	return whole_text;
}

std::string FileInput::getSample()
{
	return sample;
}

std::vector<std::vector<double> > FileInput::getVector()
{
	return vector;
}

std::vector<std::string> FileInput::breakLine(std::string line)
{
	std::transform(line.begin(), line.end(), line.begin(),
	[](unsigned char c) -> char { return c == ',' ? '.' : c; });
	
	if(line.find(";") == std::string::npos)
	{
		return {line};
	}
	
	std::vector<std::string> vector;
	
	while(line.find(";") != std::string::npos)
	{
		std::string s(line.begin(), line.begin() + line.find(";"));
		line.erase(line.begin(), line.begin() + line.find(";") + 1);
		vector.push_back(s);
	}
	
	vector.push_back(line);
	
	return vector;
}


std::vector<double> FileInput::convertToDouble(std::vector<std::string> line)
{
	std::vector<double> vector(line.size(), 1.0d);
	
	std::transform(line.begin(), line.end(), vector.begin(), 
	[this](std::string s) -> double {return this->stringToDouble(s); } );
	
	return vector;
}

double FileInput::stringToDouble(std::string str)
{
	double d;
	std::stringstream ss;
	ss << str;
	ss >> d;
	return d;
}

bool FileInput::validateStringVector(std::vector<std::string> vector_line)
{
	const std::regex regex("-?0[.][0-9]+|-?[1-9]+[.][0-9]+|0|-?[1-9][0-9]*", std::regex_constants::ECMAScript);
	std::smatch base_match;
	for(auto s : vector_line)
	{
		if(!(std::regex_match(s, base_match, regex)))
		{
			return false;
		}
	}
	return true;
}

bool FileInput::isFileEmpty()
{
	return is_file_empty;
}

bool FileInput::isDataGood()
{
	return is_data_good;
}

bool FileInput::isFileOneLine()
{
	return vector.size() == 1;
}

bool FileInput::isNumberOfDimensionsNonEqual()
{
	return is_non_equal_number_of_dimensions;
}

std::string FileInput::getErrorLine()
{
	return error_line;
}
