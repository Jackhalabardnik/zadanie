#pragma once

#include <Data.h>

#include <vector>
#include <math.h>
#include <algorithm>
#include <numeric>

class MyMath
{
public:
	MyMath();
	
	std::vector<Data> calculateStatisticPointers(std::vector<std::vector<double> > time_grain);
protected:
	double average(std::vector<double> numbers);
	
	double median(std::vector<double> numbers);
	
	double quartile1(std::vector<double> numbers);
	
	double quartile3(std::vector<double> numbers);
	
	double deviation(std::vector<double> numbers);
	
	double kurtosis(std::vector<double> numbers);
};