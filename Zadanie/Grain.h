#pragma once

#include <MyMath.h>

class Grain : private MyMath
{ 
private:
	std::string dblToStr(double d);
	std::string intToStr(int i);
public:
	std::string getText(std::vector<std::vector<double> > dimensions);
};