#pragma once

#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <regex>

class FileInput
{
public:
	void read(std::string s);
	std::string getText();
	std::string getSample();
	std::string getErrorLine();
	bool isFileEmpty();
	bool isDataGood();
	bool isFileOneLine();
	bool isNumberOfDimensionsNonEqual();
	std::vector<std::vector<double> > getVector();
	
protected:
	std::vector<double> convertToDouble(std::vector<std::string> line);
	
	std::vector<std::string> breakLine(std::string line);
	
	bool validateStringVector(std::vector<std::string> vector_line);
	
	double stringToDouble(std::string str);

private:
	std::string whole_text, sample, error_line;
	bool is_file_empty, is_data_good, is_non_equal_number_of_dimensions;
	std::vector<std::vector<double> > vector;
};