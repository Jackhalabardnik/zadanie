#include <MyMath.h>

MyMath::MyMath()
{
	
}

std::vector<Data> MyMath::calculateStatisticPointers(std::vector<std::vector<double> > time_grain)
{
	std::vector<Data> vector;
	
	std::for_each(time_grain.begin(), time_grain.end(), 
	[&vector, this](auto dim){vector.push_back({
	average(dim), quartile1(dim), median(dim), quartile3(dim), deviation(dim), kurtosis(dim)
	});});
	
	return vector;
}

double MyMath::average(std::vector<double> numbers)
{
	return std::accumulate(numbers.begin(), numbers.end(), 0.0d)/double(numbers.size());
}

double MyMath::median(std::vector<double> numbers)
{
	if(numbers.size() < 3)
	{
		return average(numbers);
	}
	
	std::sort(numbers.begin(), numbers.end());
	
	unsigned int half = numbers.size()/2;
	
	if(numbers.size() % 2 == 1)
	{
		return numbers[half];
	}
	return (numbers[half-1] + numbers[half]) / 2.0d;
}

double MyMath::quartile1(std::vector<double> numbers)
{
	if(numbers.size() == 1)
	{
		return numbers[0];
	}
	double medi = median(numbers);
	std::vector<double> half;
	for(auto x : numbers)
	{
		if(x < medi)
		{
			half.push_back(x);
		}
	}
	
	if(half.size() == 0)
	{
		half.push_back(medi);
	}
	
	return median(half);
}

double MyMath::quartile3(std::vector<double> numbers)
{
	if(numbers.size() == 1)
	{
		return numbers[0];
	}
	double medi = median(numbers);
	std::vector<double> half;
	for(auto x : numbers)
	{
		if(x > medi)
		{
			half.push_back(x);
		}
	}
	
	if(half.size() == 0)
	{
		half.push_back(medi);
	}
	
	return median(half);
}

double MyMath::deviation(std::vector<double> numbers)
{
	double avr = average(numbers);
	double sum = 0;
	
	for(auto x: numbers)
	{
		sum += pow(x - avr ,2);
	}
	
	return sqrt(sum/double(numbers.size()));
}

double MyMath::kurtosis(std::vector<double> numbers)
{
	double avr = average(numbers);
	double sum = 0;
	
	for(auto x: numbers)
	{
		sum += pow(x - avr ,4);
	}
	
	return (sum/double(numbers.size()))/(pow(deviation(numbers),4));
}

