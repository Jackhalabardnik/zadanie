#pragma once

#include <gtkmm.h>

#include <sstream>

#include <FileInput.h>

#include <MegaGrain.h>

class MainWindow : public Gtk::Window
{
public:
	MainWindow();

	virtual ~MainWindow();
	  
private:

	void setGUI();

	void run();

	void readFile();

	bool isFileEmpty(std::string uri);

	void readOutput();

	std::string getNewFileName();
	
	bool isFileNameWalid(std::string filename);

	void readTimeWindow();

	bool isValidNumber(std::string number);

	void errorMessage(std::string message);
	
	std::string intToString(int i);
	
	int stringToInt(std::string s);

	void calculate();

	
	
	bool isEuropean = false;
	
	std::string uri;

	int time_window = -1;

	FileInput file_input;
	
	MegaGrain mega_grain;
	
	Gtk::TextView sample, result;
};
