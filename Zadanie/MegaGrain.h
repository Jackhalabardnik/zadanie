#pragma once

#include <vector>

#include <Grain.h>

class MegaGrain
{
protected:
	int time_window;
	std::string itos(int i);
	std::vector<std::vector<std::vector<double> > > convertToWindows(std::vector<std::vector<double> > table);
public:
	std::string compute(std::vector<std::vector<double> > table, int timewindow);
};