#include <Grain.h>

std::string Grain::dblToStr(double d)
{
	
	d = double(int(d*1000 + 0.5d)) / 1000.0d;
	std::stringstream ss;
	ss << d;
	std::string s;
	ss >> s;
	return s;
}

std::string Grain::intToStr(int i)
{
	std::stringstream ss;
	ss << i;
	std::string s;
	ss >> s;
	return s;
}

std::string Grain::getText(std::vector<std::vector<double> > dimensions)
{
	auto data = calculateStatisticPointers(dimensions);
	
	std::string text, tab = "\t";
	
	for(Data data : data)
	{
		text += dblToStr(data.average) + " " + dblToStr(data.quartille1) 
		+ " " + dblToStr(data.median) + " " + dblToStr(data.quartille3)
		+ " " + dblToStr(data.deviation) + " ";

		text += isnan(data.kurtosis) ? intToStr(-1000) : dblToStr(data.kurtosis);
		
		text += tab;
		
	}
	
	return text;
}
