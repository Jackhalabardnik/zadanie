#include <mainwindow.h>

#include <iostream>

MainWindow::MainWindow()
{
	set_border_width(10);
	setGUI();
	sample.get_buffer()->set_text("No input given...");
	result.get_buffer()->set_text("No input given...");
}

MainWindow::~MainWindow()
{
}

void MainWindow::setGUI()
{
	auto button_run = Gtk::manage(new Gtk::Button("Choose new file"));
	
	button_run->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::run));
	
	auto scwin_sample = Gtk::manage(new Gtk::ScrolledWindow);
	scwin_sample->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
	scwin_sample->add(sample);
	scwin_sample->set_size_request(300,200);
	
	auto scwin_result = Gtk::manage(new Gtk::ScrolledWindow);
	scwin_result->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
	scwin_result->add(result);
	scwin_result->set_size_request(300,200);
	
	auto label_sample = Gtk::manage(new Gtk::Label("Sample:"));
	auto label_result = Gtk::manage(new Gtk::Label("Result:"));

	auto box = Gtk::manage(new Gtk::Box);
	
	box->set_orientation(Gtk::ORIENTATION_VERTICAL);
	
	box->pack_start(*button_run);
	box->pack_start(*label_sample);
	box->pack_start(*scwin_sample);
	box->pack_start(*label_result);
	box->pack_start(*scwin_result);
	
	add(*box);
	
	show_all_children();
}

void MainWindow::run()
{
	readFile();
}

void MainWindow::readFile()
{
	Gtk::FileChooserDialog dialog("Please choose a csv file", Gtk::FILE_CHOOSER_ACTION_OPEN);
	dialog.set_transient_for(*this);

	dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
	dialog.add_button("_Open", Gtk::RESPONSE_OK);

	auto filter_text = Gtk::FileFilter::create();
	filter_text->set_name("CSV files");
	filter_text->add_mime_type("text/csv");
	dialog.add_filter(filter_text);
	
	int result = dialog.run();
	
	if(result == Gtk::ResponseType::RESPONSE_OK)
	{
		std::string uri = dialog.get_uri();
		uri.erase(uri.begin(),uri.begin()+7);
		
		file_input.read(uri);
		
		if(file_input.isFileEmpty())
		{
			errorMessage("File is empty");
		}
		else if(file_input.isDataGood() == false)
		{
			std::string mes = "This line contains unproper chars: \"";
			mes += file_input.getErrorLine() + "\"";
			errorMessage(mes);
		}
		else if(file_input.isNumberOfDimensionsNonEqual())
		{
			std::string mes = "This line has other number of dimensions than the prevoius one: \"";
			mes += file_input.getErrorLine() + "\"";
			errorMessage(mes);
		}
		else if(file_input.isFileOneLine())
		{
			errorMessage("File contains only one line. Should contain at least 2");
		}
	
		else
		{
			readOutput();
		}
		
	}
}


bool MainWindow::isFileEmpty(std::string uri)
{
	return false;
}

void MainWindow::readOutput()
{
	Gtk::FileChooserDialog dialog("Please choose output file", Gtk::FILE_CHOOSER_ACTION_SAVE);
	dialog.set_transient_for(*this);

	auto text_view = Gtk::manage(new Gtk::TextView);
	auto scrolled_window = Gtk::manage(new Gtk::ScrolledWindow);
	auto text_label = Gtk::manage(new Gtk::Label("Opened file:"));

	scrolled_window->set_size_request(200,100);
	scrolled_window->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
	scrolled_window->add(*text_view);
	
	text_view->get_buffer()->set_text(file_input.getSample());
	
	auto radio_box = Gtk::manage(new Gtk::Box);
	auto radio_comma = Gtk::manage(new Gtk::RadioButton("European \",\""));
	auto radio_dot = Gtk::manage(new Gtk::RadioButton("USA \".\""));
	auto radio_label = Gtk::manage(new Gtk::Label("Which decimal notation would you like to use? :"));
	
	radio_comma->set_active();
	radio_comma->join_group(*radio_dot);
	
	radio_box->pack_start(*radio_label);
	radio_box->pack_start(*radio_comma);
	radio_box->pack_start(*radio_dot);
	
	dialog.get_content_area()->pack_start(*radio_box);
	dialog.get_content_area()->pack_start(*text_label);
	dialog.get_content_area()->pack_start(*scrolled_window);
	dialog.get_content_area()->show_all_children();

	int create_new_file = 123;

	dialog.add_button("Cancel", Gtk::RESPONSE_CANCEL);
	dialog.add_button("Open", Gtk::RESPONSE_OK);
	dialog.add_button("New File", create_new_file);

	auto filter_text = Gtk::FileFilter::create();
	filter_text->set_name("Text files");
	filter_text->add_mime_type("text/plain");
	dialog.add_filter(filter_text);
	
	int result = dialog.run();
	
	if(result == Gtk::ResponseType::RESPONSE_OK)
	{				
		uri = dialog.get_uri();
		uri.erase(uri.begin(),uri.begin()+7);
		
		isEuropean = radio_comma->get_active();
		
		readTimeWindow();
	}
	
	if(result == create_new_file)
	{			
		auto name = getNewFileName();
		
		if(isFileNameWalid(name) && name.size() > 0)
		{
			uri = dialog.get_current_folder_uri();
			uri.erase(uri.begin(),uri.begin()+7);
			uri += "/" + name + ".txt";
			
			isEuropean = radio_comma->get_active();
			
			readTimeWindow();
		}
		else
		{
			errorMessage("File name contains invalid characters or is empty...");
		}
	}
	
	
}

std::string MainWindow::getNewFileName()
{
	Gtk::MessageDialog dialog(*this, "Type new file name", false, Gtk::MessageType::MESSAGE_OTHER, Gtk::BUTTONS_OK_CANCEL,false);
	
	auto entry = Gtk::manage(new Gtk::Entry);
	entry->show();
	
	dialog.get_content_area()->pack_start(*entry, true, true, 1);
	
	
	int response = dialog.run();
	
	if(response == Gtk::RESPONSE_OK)
	{
		return entry->get_text();
	}
	return "";
}


bool MainWindow::isFileNameWalid(std::string filename)
{
	std::vector<std::string> chars = {"\\", "/", "|", "*", "?", "\"", ":", "<", ">", "%"};
	
	for(auto x : chars)
	{
		if(filename.find(x) != std::string::npos)
		{
			return false;
		}
	}
	return true;
}

void MainWindow::readTimeWindow()
{
	std::string message = "Type time window <2;";
	
	message += intToString(file_input.getVector().size()) + ">";
	
	Gtk::MessageDialog dialog(*this, message, false, Gtk::MessageType::MESSAGE_OTHER, Gtk::BUTTONS_OK_CANCEL,false);
	
	auto entry = Gtk::manage(new Gtk::Entry);
	entry->show();
	
	dialog.get_content_area()->pack_start(*entry, true, true, 1);

	int response = dialog.run();
	
	if(response == Gtk::RESPONSE_OK)
	{
		if(isValidNumber(entry->get_text()))
		{
			time_window = stringToInt(entry->get_text());
			calculate();
		}
		else
		{
			errorMessage("Inproper time window...");
		}
	}
}

void MainWindow::errorMessage(std::string message)
{
	Gtk::MessageDialog dialog(*this, message, false, Gtk::MessageType::MESSAGE_ERROR, Gtk::BUTTONS_OK,false);

	dialog.run();
}

void MainWindow::calculate()
{
	sample.get_buffer()->set_text(file_input.getSample());
	
	std::string text = mega_grain.compute(file_input.getVector(), time_window);
	
	if(isEuropean)
	{
		std::transform(text.begin(), text.end(), text.begin(),
		[](unsigned char c) -> char { return c == '.' ? ',' : c; });
	}
	
	result.get_buffer()->set_text(text);
	
	std::fstream file;
	file.open(uri, std::ios_base::out | std::ios_base::trunc);
	file << text;
	file.close();
}

std::string MainWindow::intToString(int i)
{
	std::string str;
	std::stringstream ss;
	ss << i;
	ss >> str;
	return str;
}

int MainWindow::stringToInt(std::string s)
{
	int i;
	std::stringstream ss;
	ss << s;
	ss >> i;
	return i;
}

bool MainWindow::isValidNumber(std::string number)
{
	const std::regex regex("[1-9][0-9]*", std::regex_constants::ECMAScript);
	std::smatch base_match;
	if(std::regex_match(number, base_match, regex) && stringToInt(number) <= int(file_input.getVector().size()) && stringToInt(number) >= 2)
	{
		return true;
	}
	return false;
}
