#include <catch.hpp>

#include <iostream>

#include <FileInput.cpp>

bool matchV(std::vector<std::vector<double> > v1, std::vector<std::vector<double> > v2)
{
	if(v1.size() != v2.size())
	{
		std::cout << "\nv1 != v2::  v1: " << v1.size() << " v2: " << v2.size() << "\n";
		
		return false;
	}
	
	for(unsigned int i=0; i< v1.size(); i++)
	{
		if(v1[i].size() != v2[i].size())
		{
			std::cout << "\nv1[i] != v2[i]::  v1[i]: " << v1[i].size() << " v2[i]: " << v2[i].size() << " i: " << i << "\n";
			return false;
		}
		for(unsigned int j = 0; j < v1[i].size(); j++)
		{
			if(v1[i][j] != v2[i][j])
			{
				std::cout << "\nv1[i][j] != v2[i][j]::  v1[i][j]: " << v1[i][j] << " v2[i][j]: " << v2[i][j] << " i: " << i << " j: "  << j << "n";
				return false;
			}
		}
	}
	
	return true;
}

TEST_CASE("File input workflow", "[FileInputTests]")
{
	FileInput fileinput;
	
	SECTION("Reads from text file")
	{
		fileinput.read("samples/plain.csv");
		CHECK(fileinput.getText() == "1.02;3.43;5.06\n4;8.2;5\n3.4;2.3;1");
		
		fileinput.read("samples/normal.csv");
		CHECK(fileinput.getText() == "1;3;5\n4;8;5\n3;2;1");
	}
	
	SECTION("Returns first 20 lines (or less) from readed text file")
	{
		fileinput.read("samples/plain.csv");
		CHECK(fileinput.getSample() == "1.02;3.43;5.06\n4;8.2;5\n3.4;2.3;1");
		
		fileinput.read("samples/big_text.csv");
		CHECK(fileinput.getSample() == "1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1");
	}
	
	SECTION("Returns vector of vectors of double from readed file")
	{
		fileinput.read("samples/plain.csv");
		CHECK(matchV(fileinput.getVector(), {{1.02d, 3.43d, 5.06d},{4.0d, 8.2d, 5.0d},{3.4d, 2.3d, 1.0d}}));
		
		fileinput.read("samples/normal.csv");
		CHECK(matchV(fileinput.getVector(), {{1.0d, 3.0d, 5.0d},{4.0d, 8.0d, 5.0d},{3.0d, 2.0d, 1.0d}}));
	}
	
	SECTION("Understands commas as dots")
	{
		fileinput.read("samples/commas.csv");
		CHECK(matchV(fileinput.getVector(), {{1.02d, 3.43d, 5.06d},{4.0d, 8.2d, 5.0d},{3.4d, 2.3d, 1.0d}}));
	}
}

TEST_CASE("Validation", "[FileInputTest]")
{
	FileInput input;
	
	SECTION("Reacts for empty file")
	{
		input.read("samples/empty.csv");
		CHECK(input.isFileEmpty());
	}
	
	SECTION("Reacts for empty file with enters")
	{
		input.read("samples/empty_enters.csv");
		CHECK(input.isFileEmpty());
	}
	
	SECTION("Reacts for file with only one line")
	{
		input.read("samples/one_line.csv");
		CHECK(input.isFileOneLine());
	}
	
	SECTION("Reacts for many bad mistakes")
	{
		input.read("samples/unwanted_chars.csv");
		CHECK(input.isDataGood() == false);
		CHECK(input.getErrorLine() == "1.a0;3.5;3.3");
		
		input.read("samples/many_dots.csv");
		CHECK(input.isDataGood() == false);
		CHECK(input.getErrorLine() == "1,.1;123;345.3");
		
		input.read("samples/non-equal_lines.csv");
		CHECK(input.isNumberOfDimensionsNonEqual() == true);
		CHECK(input.getErrorLine() == "2;3");
	}
	
	SECTION("Doesnt react for proper data")
	{
		input.read("samples/plain.csv");
		CHECK(input.isDataGood() == true);
	}
}