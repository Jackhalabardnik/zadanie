#include <catch.hpp>

#include <iostream>

#include <MegaGrain.cpp>
#include <Grain.cpp>

using triple_double = std::vector<std::vector<std::vector<double> > >;

bool matchV(triple_double v1, triple_double v2)
{
	if(v1.size() != v2.size())
	{
		std::cout << "v1 != v2 : " << v1.size() << " != " << v2.size() << std::endl;
		return false;
	}
	for(unsigned int i=0;i < v1.size(); i++)
	{
		if(v1[i].size() != v2[i].size())
		{
			std::cout << "i: " << i << " v1[i] != v2[i] : " << v1[i].size() << " != " << v2[i].size() << std::endl;
			return false;
		}
		
	
		for(unsigned int j=0; j< v1[i].size(); j++)
		{
			if(v1[i][j].size() != v2[i][j].size())
			{
				std::cout << "i: " << i << " j:" << j << " v1[i][j] != v2[i][j] : " << v1[i][j].size() << " != " << v2[i][j].size() << std::endl;
				return false;
			}
			
			for(unsigned int k=0; k< v1[i][j].size(); k++)
			{
				if(v1[i][j][k] != v2[i][j][k])
				{
					std::cout << "i: " << i << " j: " << j << " k: " << k << " v1[i][j][k] != v2[i][j][k] : " << v1[i][j][k] << " != " << v2[i][j][k] << std::endl;
					return false;
				}
			}
		}
	}
	
	return true;
}

class MockMegaGrain : public MegaGrain
{
public:
	triple_double convertToWindows(std::vector<std::vector<double> > table, int time_window)
	{
		MegaGrain::time_window = time_window;
		return MegaGrain::convertToWindows(table);
	}
};

TEST_CASE("MegaGrain divde array into separate grains", "[MegaGrainTest]")
{
	MockMegaGrain MG;
	
	SECTION("For time window less than max x table size")
	{
		CHECK(matchV(MG.convertToWindows({{2,3},{4,5},{6,8}}, 2), {{{2,4},{3,5}}, {{4,6},{5,8}}}));
	}
	
	SECTION("Fot time window exual max x table size")
	{
		CHECK(matchV(MG.convertToWindows({{2,3},{4,5},{6,8}}, 3), {{{2,4,6},{3,5,8}}}));
	}
}