#include <catch.hpp>

#include <iostream>

#include <MyMath.cpp>

#include <Data.h>

double cut_last(double d)
{
	std::stringstream ss;
	ss.precision(10);
	ss << d;
	ss >> d;
	return d;
}

class MockMath : public MyMath
{
public:
	double average(std::vector<double> numbers) 
	{ 
		return MyMath::average(numbers); 
	}
	
	double median(std::vector<double> numbers) 
	{ 
		return MyMath::median(numbers); 
	}
	
	double quartile1(std::vector<double> numbers) 
	{ 
		return MyMath::quartile1(numbers); 
	}
	
	double quartile3(std::vector<double> numbers) 
	{ 
		return MyMath::quartile3(numbers); 
	}
	
	double deviation(std::vector<double> numbers) 
	{ 
		return MyMath::deviation(numbers); 
	}
	
	double kurtosis(std::vector<double> numbers) 
	{ 
		return MyMath::kurtosis(numbers); 
	}
};

bool matchData(std::vector<Data> v1, std::vector<Data> v2)
{
	if(v1.size() != v2.size())
	{
		std::cout << "v1 != v2 : " << v1.size() << " " << v2.size() << std::endl;
		return false;
	}
	for(unsigned int i=0;i < v1.size(); i++)
	{
		if(v1[i] != v2[i])
		{
			std::cout << "v1[i] != v2[i]" << " i: " << i << " v1[i]: " << v1[i].toString() << " v2[i]: " << v2[i].toString() << std::endl;
			return false;
		}
	}
	return true;
}


TEST_CASE("Math: average", "[MyMathTests]")
{
	MockMath calc;
	
	CHECK(1.0d == calc.average({1}));
	CHECK(2.0d == calc.average({1, 3}));
	CHECK(1.5d == calc.average({2.5d, 0.5d}));
	CHECK(3.0d == calc.average({1, 2, 6}));
}

TEST_CASE("Math: median", "[MyMathTests]")
{
	MockMath calc;
	
	CHECK(1.0d == calc.median({1}));
	CHECK(2.0d == calc.median({1, 3}));
	CHECK(4.25d == calc.median({2, 4.5d, 8, 10000, 3, 4, 2, 4.5d}));
	CHECK(4.5d == calc.median({2, 4.5d, 8, 10000, 3}));
}

TEST_CASE("Math: deviation", "[MyMathTests]")
{
	MockMath calc;
	
	CHECK(0.0d == calc.deviation({1}));
	CHECK(1.0d == calc.deviation({1, 3}));
	CHECK(10.88163591d == cut_last(calc.deviation({18, 30, 21, 42, 55, 34, 45, 39, 38, 25})));
}

TEST_CASE("Math: quartile 1", "[MyMathTests]")
{
	MockMath calc;
	
	CHECK(1.0d == calc.quartile1({1}));
	CHECK(1.0d == calc.quartile1({1, 1}));
	CHECK(1.0d == calc.quartile1({1, 3}));
	CHECK(4.0d == calc.quartile1({5,7,4,4,6,2,8}));
	CHECK(3.0d == calc.quartile1({1, 3, 3, 4, 5, 6, 6, 7, 8, 8}));
}

TEST_CASE("Math: quartile 3", "[MyMathTests]")
{
	MockMath calc;
	
	CHECK(1.0d == calc.quartile3({1}));
	CHECK(1.0d == calc.quartile3({1, 1}));
	CHECK(3.0d == calc.quartile3({1, 3}));
	CHECK(7.0d == calc.quartile3({5,7,4,4,6,2,8}));
	CHECK(7.0d == calc.quartile3({1, 3, 3, 4, 5, 6, 6, 7, 8, 8}));
}

TEST_CASE("Math: kurtosis", "[MyMathTests]")
{
	MockMath calc;
	CHECK(isnan(calc.kurtosis({1,1,1})));
	CHECK(1.0d == calc.kurtosis({5,7}));
	CHECK(1.5d == cut_last(calc.kurtosis({5,7,4})));
	CHECK(2.039833532d == cut_last(calc.kurtosis({5,7,4,4,6,2,8})));
	CHECK(2.002070082d == cut_last(calc.kurtosis({1, 3, 3, 4, 5, 6, 6, 7, 8, 8})));
}

TEST_CASE("Math: calculate one grain", "[MyMathTests]")
{
	MockMath calc;
	std::vector<Data> wanted_result= {{2.6666666667d,1.0d,2.5d,4.0d,1.4907119850d,1.635d},{6.5d,5,6.5d,8,1.707825128d,1.731428571d},{2,1,1.75d,3,1.190238071d, 1.920415225d}};
	std::vector<std::vector<double> > input = {{1,2,1,3,4,5},{7,6,8,9,5,4},{1.5,0.5,3,4,1,2}};
	
	CHECK(matchData(wanted_result, calc.calculateStatisticPointers(input)));
}
