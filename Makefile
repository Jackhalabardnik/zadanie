.PHONY: clean All

All:
	@echo "----------Building project:[ Zadanie - Release ]----------"
	@cd "Zadanie" && "$(MAKE)" -f  "Zadanie.mk"
clean:
	@echo "----------Cleaning project:[ Zadanie - Release ]----------"
	@cd "Zadanie" && "$(MAKE)" -f  "Zadanie.mk" clean
